#include "mpi.h"
#include <stdio.h>
#include <iostream>
#include <ctime>
#include <fstream>

using namespace std;

const int maxProcesses = 10;
const int N = 10000; // количество работников
const int maxNameLen = 100; // макс. длина имени

const int maxHiredYear = 2016;
const int randMaxHiredYear = 16;
const int maxBornYear = 1930;
const int randMaxBornYear = 65;
const int hiredYearsCondition = 5;

int numtasks, rank, SOURCE = 0;
char randNamesLength = 5;
char randNames[][maxNameLen] = {"Ivan", "Stepan", "Petr", "Alexey", "Karl"};
char randSurnames[][maxNameLen] = {"Ivanov", "Stepanov", "Petrov", "Alexeyeev", "Karlov"};


char names[maxProcesses][N][maxNameLen];
int bornYears[maxProcesses][N];
int hiredYears[maxProcesses][N];
char markedWorkers[maxProcesses][N];


char recvNames[N][maxNameLen];
int recvBornYears[N];
int recvHiredYears[N];


void printToFile(char *fname) {
    FILE *fp;

    fp = fopen(fname, "w");

    int counter = 0;
    for (int process = 0; process < numtasks; ++process) {
        for (int i = 0; i < N; ++i) {
            if (markedWorkers[process][i] != 'x') {
                char * name = "Name: ""/n";
                strcat(name, names[process][i]);


                fprintf(fp, name);
                out << "Born year: " << bornYears[process][i] << endl;
                out << "Was hired in " << hiredYears[process][i] << endl;
                out << endl;
                counter++;
            }
        }
    }

    out << "TOTAL AMOUNT:" << counter << endl;
    out.flush();
    out.close();
}

void initiateWorkers() {
    for (int process = 0; process < numtasks; ++process) {
        for (int i = 0; i < N; ++i) {
            char name[maxNameLen], surname[maxNameLen], common[maxNameLen];
            strcpy(name, randNames[rand() % randNamesLength]);
            strcpy(surname, randSurnames[rand() % randNamesLength]);
            strcat(name, " ");

            strcpy(names[process][i], strcat(name, surname));
            bornYears[process][i] = maxBornYear + rand() % randMaxBornYear;
            hiredYears[process][i] = maxHiredYear - rand() % randMaxHiredYear;
        }

    }
}

void markWorkers(int rank) {
    for (int i = 0; i < N; ++i) {
        if (maxHiredYear - recvHiredYears[i] < hiredYearsCondition) {
            markedWorkers[rank][i] = 'x';
        }
    }
}

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

    if (rank == 0) {
        initiateWorkers();
        printToFile("initial.txt");
    }


    MPI_Scatter(names, N * maxNameLen, MPI_CHAR, recvNames, N * maxNameLen, MPI_CHAR, SOURCE, MPI_COMM_WORLD);
    MPI_Scatter(bornYears, N, MPI_INT, recvBornYears, N, MPI_INT, SOURCE, MPI_COMM_WORLD);
    MPI_Scatter(hiredYears, N, MPI_INT, recvHiredYears, N, MPI_INT, SOURCE, MPI_COMM_WORLD);

    markWorkers(rank);

    MPI_Gather(markedWorkers[rank], N, MPI_CHAR, markedWorkers, N, MPI_CHAR, SOURCE, MPI_COMM_WORLD);

    if (rank == SOURCE) {
        printToFile("filtered.txt");
    }

    MPI_Finalize();
}
